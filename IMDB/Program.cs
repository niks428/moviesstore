﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDB.Domain;

namespace IMDB
{
    class Program
    {
        static void Main(string[] args)
        {

            var serviceClass = new ServiceClass();

            string choice;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.ResetColor();

            do
            {
                Console.WriteLine("\n\t\tMovie Store");
                Console.WriteLine("1. List Movie");
                Console.WriteLine("2. Add Movie");
                Console.WriteLine("3. Add Actor");
                Console.WriteLine("4. Add Producer");
                Console.WriteLine("5. Delete Movie");
                Console.WriteLine("6. Exit");
                Console.WriteLine("\nEnter Your Choice: ");

                string input = null;
                choice = Console.ReadLine();

                Console.WriteLine("\n ===================================================\n ");

                switch (choice)
                {
                    case "1": //show movie
                        try
                        {
                            var movieList = serviceClass.GetMovies();
                            int index = 1;
                            if (movieList.Count() < 1)
                            {
                                throw new Exception("No Movie present ");
                            }
                            else
                            {
                                Console.WriteLine("Available Movies :");
                                foreach (var movies in movieList)
                                {
                                    Console.WriteLine("{0}. {1}", index++, movies.MovieName);
                                }

                            }

                        }
                        catch (Exception exception)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(exception.Message);
                            Console.ResetColor();
                        }
                        break;

                    case "2":
                        try
                        {
                            if (!serviceClass.IsActorAvailable())
                            {
                                throw new Exception("\nNo Actors Available. Add Actors First");
                            }

                            if (!serviceClass.IsProducerAvailable())
                            {
                                throw new Exception("\nNo Producer Available. Add Producer First");
                            }

                            bool check = false;
                            string name;
                            int year = 0;

                            //Get Movie Name
                            do
                            {
                                Console.WriteLine("\nEnter Movie Name (Enter 'Exit' to cancel)");
                                name = GetInput();
                                if (IsExit(name))
                                {
                                    throw new Exception("Cancelled");
                                }

                                if (string.IsNullOrEmpty(name))
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Name cannot be empty, Enter Again");
                                    Console.ResetColor();

                                }
                                else
                                {
                                    check = true;
                                }
                            } while (!check);

                            //Get Year of Release
                            check = false;
                            do
                            {
                                Console.WriteLine("\nEnter Year of Release (0 to cancel)");
                                var yearStr = GetInput();
                                if (yearStr.Equals("0"))
                                {
                                    throw new Exception("Cancelled");
                                }
                                if (string.IsNullOrEmpty(yearStr))
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Year of  Release cannot be empty, Enter Again");
                                    Console.ResetColor();
                                }

                                try
                                {
                                    year = int.Parse(yearStr);
                                    if (year >= 1900 && year <= DateTime.Now.Year)
                                    {
                                        check = true;
                                    }
                                    else
                                    {
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Enter year in range [1900-{0}]", DateTime.Now.Year);
                                        Console.ResetColor();
                                    }

                                }
                                catch (FormatException)
                                {

                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Entered Year '{0}' is not Valid. Enter Again", yearStr);
                                    Console.ResetColor();


                                }

                            } while (!check);

                            //Get Plot
                            string plot = GetPlot();
                            if (IsExit(plot))
                            {
                                throw new Exception("Cancelled");
                            }

                            //List Actors
                            Console.WriteLine("\n");
                            var actorsList = serviceClass.GetAllActors();  
                            int i = 1;
                            foreach (var actor in actorsList)
                            {
                                Console.Write("{0}. {1}\t", i, actor.Name);
                                i++;
                            }
                            //Choose Actor
                            var movieActors = new List<Actor>();
                            bool flag = false;
                            do
                            {
                                input = null;
                                input = ChooseId("Actors");
                                if (input == "0")
                                {
                                    throw new Exception("Cancelled");
                                }

                                try
                                {
                                    movieActors = serviceClass.ChooseActors(input, year, ref flag);
                                }
                                catch (FormatException)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Choose valid Actor number(s) from Above");
                                    Console.ResetColor();
                                    movieActors.Clear();

                                }
                                catch (IndexOutOfRangeException)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Choose valid Actor number(s) from Above");
                                    Console.ResetColor();
                                    movieActors.Clear();

                                }

                            } while (!flag);

                            //List Producers
                            Console.WriteLine("\n");
                            var producerList = serviceClass.GetAllProducers();
                            i = 1;
                            foreach (var producer in producerList)
                            {
                                Console.Write("{0}. {1}\t", i, producer.Name);
                                i++;
                            }
                            //Choose Producer
                            flag = false;
                            var movieProducer = new List<Producer>();
                            do
                            {
                                input = null;
                                input = ChooseId("a Producer");
                                if (input == "0")
                                {
                                    throw new Exception("Cancelled");
                                }

                                try
                                {
                                    movieProducer = serviceClass.ChooseProducers(input, year,ref flag);
                                }
                                catch (FormatException)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Choose valid Producer number from Above");
                                    Console.ResetColor();
                                    movieProducer.Clear();

                                }
                                catch (IndexOutOfRangeException)
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Choose valid Producer number from Above");
                                    Console.ResetColor();
                                    movieProducer.Clear();

                                }

                            } while (!flag);

                            //Add movie to db
                            serviceClass.AddMovie(name, year, plot, movieActors, movieProducer[0]);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("{0} Added Successfully.", name);
                            Console.ResetColor();

                        }
                        catch (Exception exception)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(exception.Message);
                            Console.ResetColor();
                        }
                        break;

                    case "3":
                        try
                        {

                            Console.WriteLine("Enter Actor Name (Enter  'Exit' to cancel) :");
                            var name = GetInput();
                            if (IsExit(name))
                            {
                                throw new Exception("Cancelled");
                            }

                            var dob = GetDateOfBirth(name);
                            if (DateTime.Compare(dob, new DateTime(1, 1, 1)) == 0)
                            {
                                throw new Exception("Cancelled");

                            }

                            serviceClass.AddActor(name, dob);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("{0} Added Successfully.", name);
                            Console.ResetColor();


                        }
                        catch (Exception exception)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(exception.Message);
                            Console.ResetColor();
                        }
                        break;

                    case "4":

                        try
                        {
                            Console.WriteLine("Enter Producer Name (Enter  'Exit' to cancel) :");
                            var name = GetInput();
                            if (IsExit(name))
                            {
                                throw new Exception("Cancelled");

                            }

                            var dob = GetDateOfBirth(name);
                            if (DateTime.Compare(dob, new DateTime(1, 1, 1)) == 0)
                            {
                                throw new Exception("Cancelled");
                            }
                            serviceClass.AddProducer(name, dob);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("{0} Added Successfully.", name);
                            Console.ResetColor();

                        }
                        catch (Exception exception)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(exception.Message);
                            Console.ResetColor();
                        }
                        break;

                    case "5":

                        try
                        {
                            Console.WriteLine("Choose Movie to Delete");
                            //list all movies
                            var movieList = serviceClass.GetMovies();
                            int index = 1;
                            if (movieList.Count() < 1)
                            {
                                throw new Exception("No Movie present ");
                            }
                            else
                            {
                                foreach (var movies in movieList)
                                {
                                    Console.WriteLine("{0}. {1}", index++, movies.MovieName);
                                }

                            }
                            input = null;
                            input = ChooseId("Movie");
                            if (input == "0")
                            {
                                throw new Exception("Cancelled");
                            }

                            serviceClass.DeleteMovie(input);
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Removed Successfully");
                            Console.ResetColor();

                        }
                        catch (Exception exception)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(exception.Message);
                            Console.ResetColor();
                        }



                        break;
                    case "6":
                        Console.Clear();
                        Console.WriteLine("Bye! Bye!");
                        return;


                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Enter Valid input");
                        Console.ResetColor();
                        break;
                }

                Console.WriteLine("\nEnter any key to Continue...");
                Console.ReadKey();
                Console.Clear();

            } while (choice != "6");

        }

        private static bool IsExit(string input)
        {
            if (string.Equals(input, "Exit", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;

        }

        private static string GetInput()
        {
            var input = Console.ReadLine();
            return input;
        }

        private static DateTime GetDateOfBirth(string item)
        {

            DateTime dob = new DateTime(1, 1, 1);
            string dobStr;
            bool valid = false;

            Console.WriteLine("\nEnter {0} DOB in (MM-DD-YYYY) (Enter 'Exit' to cancel)", item);


            do
            {
                try
                {
                    dobStr = Console.ReadLine();

                    if (IsExit(dobStr))
                    {
                        return dob;
                    }

                    dob = DateTime.Parse(dobStr);
                    if (DateTime.Compare(dob, DateTime.Today) > 0) //(dob,new DateTime(1885,01,01))<0  //min DOB acceptable

                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\nDOB Cannot be of Future ");
                        Console.ResetColor();
                    }
                    else
                    {
                        valid = true;//validated & no exception thrown

                    }

                }
                catch (Exception)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("\nInvalid Date Format , Enter Again in  (MM-DD-YYYY) format ");
                    Console.ResetColor();
                }

            } while (!valid);

            return dob;
        }


        private static string GetPlot()
        {
            Console.WriteLine("\nEnter Plot (Press Enter twice to finish) (Enter 'Exit' to cancel)");
            string temp_plot;
            string plot = "";
            temp_plot = Console.ReadLine();
            if (IsExit(temp_plot))
            {
                return temp_plot;
            }
            plot += temp_plot;
            do
            {
                temp_plot = Console.ReadLine();
                plot += temp_plot;
            } while (!String.IsNullOrWhiteSpace(temp_plot));

            return plot;
        }

        private static string ChooseId(string item)
        {

            string input=null;
            Console.WriteLine("\nChoose {0} From List (0 to Cancel):",item);
            input = Console.ReadLine();
            return input;

        }
    }




}
