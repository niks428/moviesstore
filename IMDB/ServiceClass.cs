﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using IMDB.Domain;
using IMDB.Repository;

namespace IMDB
{
    public class ServiceClass
    {


        private ActorRepo _actorRepo;
        private ProducerRepo _producerRepo;
        private MovieRepo _movieRepo;

        public ServiceClass()
        {
            _actorRepo = new ActorRepo();
            _producerRepo = new ProducerRepo();
            _movieRepo = new MovieRepo();

        }

        public bool IsActorAvailable()
        {
            var actorsList = _actorRepo.GetAll();
            if (!actorsList.Any())
            {
                return false;

            }
            return true;

        }

        public bool IsProducerAvailable()
        {
            var producerList = _producerRepo.GetAll();
            if (!producerList.Any())
            {
                return false;

            }
            return true;

        }

        public List<Movie> GetMovies()
        {
            return _movieRepo.GetMovies();
        }

        public void AddMovie(string name, int year, string plot, List<Actor> movieActors, Producer producer)
        {


            _movieRepo.AddMovie(name, year, plot, movieActors, producer);

        }

        public List<Actor> GetAllActors()
        {
            return _actorRepo.GetAll();
        }

        public Actor GetActor(int index)
        {
            return _actorRepo.GetAt(index);
        }

        public List<Actor> GetChoosenActors(string input)
        {
            List<Actor> actorsList = new List<Actor>();
            if (string.IsNullOrEmpty(input))
            {
                return actorsList;
            }
            var choosen = input.Split();
            foreach (var index in choosen)
            {
                actorsList.Add(_actorRepo.GetAt(int.Parse(index)));

            }
            return actorsList;
        }

        public void AddActor(string name, DateTime dob)
        {
            Actor actor = new Actor
            {
                Name = name,
                Dob = dob
            };
            _actorRepo.Add(actor);

        }

        public List<Producer> GetAllProducers()
        {
            return _producerRepo.GetAll();
        }

        public List<Producer> GetChoosenProducers(string input)
        {
            List<Producer> producersList = new List<Producer>();
            var choosen = input.Split();
            if (string.IsNullOrEmpty(input))
            {
                return producersList;
            }
            foreach (var index in choosen)
            {
                producersList.Add(_producerRepo.GetAt(int.Parse(index)));

            }
            return producersList;
        }

        public Producer GetProducer(string index)
        {
            return _producerRepo.GetAt(int.Parse(index));
        }

        public void AddProducer(string name, DateTime dob)
        {
            Producer producer = new Producer
            {
                Name = name,
                Dob = dob
            };
            _producerRepo.Add(producer);


        }

        public void DeleteMovie(string input)
        {
            var choosen = input.Split();
            foreach (var index in choosen)
            {
                _movieRepo.RemoveAt(int.Parse(index));
            }
        }

        public List<Actor> ChooseActors(string input, int movieYear, ref bool flag)
        {
            List<Actor> movieActors = new List<Actor>();
            
            var choosen = input.Split();

            foreach (var index in choosen)
            {
                //verify actor Dob and movie year of Release 
                // one or more selected Born after Movie Release then cancel 
                var intIndex = int.Parse(index);
                if (_actorRepo.GetAt(intIndex).Dob.Year > movieYear)
                {
                    throw new Exception($"{index}. {_actorRepo.GetAt(intIndex).Name} Born after Movie Release.Cancelled");
                }
                else
                {
                    movieActors.Add(_actorRepo.GetAt(int.Parse(index)));
                }

            }
            
            if (!movieActors.Any())
            {
                throw new Exception("Atleast 1 Actor Required.\n Cancelled");
            }
            flag = true; //when all choosen are valid
            return movieActors;

        }

        public List<Producer> ChooseProducers(string input, int movieYear,ref bool flag)
        {
            var movieProducer = new List<Producer>();
            var choosen = input.Split();

            foreach (var index in choosen)
            {
                //only one (first) id choosen
                //verify producer Dob and movie yearof Release
                //Throws if invalid index choosen
                if (_producerRepo.GetAt(int.Parse(index)).Dob.Year > movieYear)
                {
                    throw new Exception($"{choosen[0]}. {_producerRepo.GetAt(int.Parse(index)).Name} Born after Movie Release.Cancelled ");

                }
                else
                {
                    movieProducer.Add(_producerRepo.GetAt(int.Parse(choosen[0])));
                }
            }

            if (!movieProducer.Any() || movieProducer.Count() > 1)
            {
                throw new Exception("Only 1 Producer Required.\n Cancelled");

            }

            flag = true; //when all choosen are valid
            return movieProducer;

        }

    }
}
