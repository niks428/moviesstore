﻿using System.Collections.Generic;

namespace IMDB.Domain
{
    public class Movie
    {
        public string MovieName { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        public List<Actor> Actors { get; set; }
        public Producer Producer {get; set;}
    }
}
