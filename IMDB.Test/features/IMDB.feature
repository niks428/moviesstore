﻿Feature: Calculator
	Simple Movie Liberary Management

Link to a feature: [Calculator](IMDB.Test/Features/Calculator.feature)
***Further read***: **[Learn more about how to generate Living Documentation](https://docs.specflow.org/projects/specflow-livingdoc/en/latest/LivingDocGenerator/Generating-Documentation.html)**

@add-actor
Scenario: Add valid actor to database
	Given name is 'Amit'
	And dob is '09-10-1989'
	When add actor to the database
	Then Actor table looks like
	| Name   | Dob			|
	| Amit	 | 09-10-1989	|


@add-producer
Scenario: Add valid producer to database
	Given name is 'Mohit'
	And dob is '05-12-1999'
	When add producer to the database
	Then Producer table looks like
	| Name   | Dob        |
	| Mohit	 | 05-12-1999 |

@add-empty-movie
Scenario: Add new movie to empty movie database
	Given name is 'Iron Man'
	And year of realease is '2012'
	And plot is 'Super hero movie'
	And actor is '1 2' 
	And producer is '1'
	When add movie to the database
	Then Movie table looks like
	| MovieName | YearOfRelease | Plot             |
	| Iron Man  | 2012          | Super hero movie |
	And Movie-actors table looks like
	| Name | Dob        |
	| Nik1 | 10-01-1985 |
	| Nik2 | 12-12-1999 |
	And Movie-Producer table looks like
	| Name | Dob        |
	| Pro1 | 10-01-1995 |

@add-non-empty-movie
Scenario: Add new movie to non empty movie database
	Given name is 'Iron Man'
	And year of realease is '2012'
	And plot is 'Super hero movie'
	And actor is '1 2' 
	And producer is '1'
	When add movie to the database
	Then Movie table looks like
	| MovieName | YearOfRelease | Plot               |
	| Troy      | 2001          | War Movie          |
	| Rudy      | 1995          | Motivational Movie |
	| Iron Man  | 2012          | Super hero movie   |

	And Movie-actors table looks like
	| Name | Dob        |
	| Nik1 | 10-01-1985 |
	| Nik2 | 12-12-1999 |
	And Movie-Producer table looks like
	| Name | Dob        |
	| Pro1 | 10-01-1995 |

@add-empty-movie
Scenario: Don't Add Actor born after movie Release
	Given name is 'Orphan'
	And year of realease is '1995'
	And plot is 'Crime Thriller'
	And actor is '2' 
	And producer is '1'
	When add movie to the database
	Then error should occur '2. Nik2 Born after Movie Release.Cancelled'
	Then Movie table looks like
	| MovieName | YearOfRelease | Plot             |
	And Movie-actors table looks like
	| Name | Dob        |
	And Movie-Producer table looks like
	| Name | Dob        |
	



@delete-movie
Scenario: Delete movie from database
	Given movie is '1'
	When delete movie from database
	Then Movie table looks like
	| MovieName | YearOfRelease | Plot					| 
	| Rudy      | 1995          | Motivational Movie	|




