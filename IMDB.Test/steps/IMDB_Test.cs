﻿using TechTalk.SpecFlow;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow.Assist;
using Xunit;
using IMDB.Domain;

namespace IMDB.Test.Steps
{
    [Binding]
    public sealed class IMDB_Test
    {
        private string _name;
        private DateTime _dob;
        private int _yearOfRelease;
        private string _plot;
        private ServiceClass _serviceClass;
        private List<Actor> _actors;
        private List<Producer> _producer;
        private string _choosen, _choosenActors, _choosenProducer;
        private List<Movie> _movies;
        private Exception _exception;
        private bool _flag;

        public IMDB_Test()
        {
            _serviceClass = new ServiceClass();
            _actors = new List<Actor>();
            _producer = new List<Producer>();
            _movies = new List<Movie>();
            _flag = false;
        }

        [Given(@"name is '(.*)'")]
        public void GivenNameIs(string name)
        {
            _name = name;
        }

        [Given(@"dob is '(.*)'")]
        public void GivenDobIs(DateTime dob)
        {
            _dob = dob;
        }

        [When(@"add actor to the database")]
        public void WhenAddActorToTheDatabase()
        {
            _serviceClass.AddActor(_name, _dob);
        }

        [Then(@"Actor table looks like")]
        public void ThenActorTableLooksLike(Table table)
        {
            var actorList = _serviceClass.GetAllActors();
            table.CompareToSet(actorList);
        }

        [When(@"add producer to the database")]
        public void WhenAddProducerToTheDatabase()
        {
            _serviceClass.AddProducer(_name, _dob);
        }

        [Then(@"Producer table looks like")]
        public void ThenProducerTableLooksLike(Table table)
        {
            var producerList = _serviceClass.GetAllProducers();
            table.CompareToSet(producerList);
        }

        [Given(@"year of realease is '(.*)'")]
        public void GivenYearOfRealeaseIs(int year)
        {
            _yearOfRelease = year;
        }

        [Given(@"plot is '(.*)'")]
        public void GivenPlotIs(string plot)
        {
            _plot = plot;
        }

        [Given(@"actor is '(.*)'")]
        public void GivenActorIs(string choosen)
        {
            _choosenActors = choosen;
            
        }

        [Given(@"producer is '(.*)'")]
        public void GivenProducerIs(string choosen)
        {
            _choosenProducer = choosen;
            
        }

        [When(@"add movie to the database")]
        public void WhenAddMovieToTheDatabase()
        {
            try
            {
                _actors = _serviceClass.ChooseActors(_choosenActors, _yearOfRelease,ref _flag);
                _producer = _serviceClass.ChooseProducers(_choosenProducer, _yearOfRelease, ref _flag);
                _serviceClass.AddMovie(_name, _yearOfRelease, _plot, _actors, _producer[0]);

            }
            catch (Exception exception)
            {
                _choosenActors = "";
                _choosenProducer = "";
                _exception = exception;
            }
        }


        [Then(@"Movie table looks like")]
        public void ThenMovieTableLooksLike(Table table)
        {
            _movies = _serviceClass.GetMovies();
            table.CompareToSet(_movies);
        }

        [Then(@"Movie-actors table looks like")]
        public void ThenMovie_ActorsTableLooksLike(Table table)
        {
            _actors = _serviceClass.GetChoosenActors(_choosenActors);
            table.CompareToSet(_actors);
        }

        [Then(@"Movie-Producer table looks like")]
        public void ThenMovie_ProducerTableLooksLike(Table table)
        {
            _producer = _serviceClass.GetChoosenProducers(_choosenProducer);
            table.CompareToSet(_producer);
        }


        [Given(@"movie is '(.*)'")]
        public void GivenMovieIs(string input)
        {
            _choosen = input;
        }

        [When(@"delete movie from database")]
        public void WhenDeleteMovieFromDatabase()
        {
            _serviceClass.DeleteMovie(_choosen);
        }

        [Then(@"error should occur '(.*)'")]
        public void ThenErrorShouldOccur(string message)
        {
            Assert.Equal(message, _exception.Message);
        }


        [BeforeScenario("add-empty-movie")]
        public void AddSampleActorProducer()
        {   //MM-DD-YYYY
            _serviceClass.AddActor("Nik1", DateTime.Parse("10-01-1985"));
            _serviceClass.AddActor("Nik2", DateTime.Parse("12-12-1999"));
            _serviceClass.AddActor("Nik3", DateTime.Parse("05-25-1970"));

            _serviceClass.AddProducer("Pro1", DateTime.Parse("10-01-1995"));
            _serviceClass.AddProducer("Pro2", DateTime.Parse("09-20-2005"));
            _serviceClass.AddProducer("Pro3", DateTime.Parse("01-19-1982"));
        }


        [BeforeScenario("delete-movie")]
        [BeforeScenario("@add-non-empty-movie")]
        public void AddSampleMovieForAdd()
        {
            //to get actors-producers for addSampleMovie [before scenario]
            AddSampleActorProducer();

            _actors = _serviceClass.GetAllActors();
            _producer = _serviceClass.GetAllProducers();

            List<Actor> mActor = new List<Actor> { _actors[0], _actors[1] };
            List<Producer> mProducer = new List<Producer> { _producer[0]};
            

            _serviceClass.AddMovie("Troy", 2001, "War Movie", mActor, mProducer[0]);

            mActor.Clear();
            mProducer.Clear();
            mActor.Add(_actors[1]);
            mProducer.Add(_producer[2]);

            _serviceClass.AddMovie("Rudy", 1995, "Motivational Movie", mActor, mProducer[0]);

        }



    }
}
