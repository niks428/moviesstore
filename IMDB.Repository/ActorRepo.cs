﻿using System.Collections.Generic;
using IMDB.Domain;

namespace IMDB.Repository
{
    public class ActorRepo

    {
        private List<Actor> _actorsList;

        public ActorRepo()
        {
            _actorsList = new List<Actor>();
        }

        public void Add(Actor actor)
        {
            _actorsList.Add(actor);
           
        }

        public List<Actor> GetAll() 
        {
            return _actorsList;
        }

        public Actor GetAt(int index)
        {
            return _actorsList[index - 1];
        }



    }

}
