﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDB.Domain;


namespace IMDB.Repository
{
    public class MovieRepo
    {
        private List<Movie> _movieList;

        public MovieRepo()
        {
            _movieList = new List<Movie>();
        }


        public List<Movie> GetMovies()
        {
            return _movieList;
        }

        public void AddMovie(string name,int year,string plot,List<Actor> actors,Producer producer)
        {
            _movieList.Add(new Movie
            {
                MovieName = name,
                YearOfRelease = year,
                Plot=plot,
                Actors = actors,
                Producer = producer
            });
        }

        public void RemoveAt(int index)
        {
            _movieList.RemoveAt(index - 1);
            
        }


    }
}
