﻿using System;
using System.Collections.Generic;
using System.Linq;
using IMDB.Domain;

namespace IMDB.Repository
{
    public class ProducerRepo
    {
        private List<Producer> _producerList;

        public ProducerRepo()
        {
            _producerList = new List<Producer>();
        }

        public void Add(Producer producer)
        {
            _producerList.Add(producer);
           

        }

       public List<Producer> GetAll()
        {
            return _producerList;
        }

        public Producer GetAt(int index)
        {
            return _producerList[index - 1];
        }

       
    }

}
